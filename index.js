require('dotenv').config()

const express = require('express')
const os = require('os')
const app = express()
const mysql = require('mysql')

app.get('/', (req, res) => res.send(`Hello world from ${os.hostname()}!`))

app.listen(8080)
